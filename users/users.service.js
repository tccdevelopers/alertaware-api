//users.service.js
/**
 * 
 *Here is the file with all methods to create, retrieve, modify and delete user(s) entities from database 
 *
 */

 var User = require("../users/users.schema");
 var Device = require('../devices/devices.schema');
 //Service's array to export
 var logger = require('mm-node-logger')(module);
var service = {};
service.getListUsers = getListUsers;
service.getUser = getUser;
service.deleteUser = deleteUser;
service.insertUser = insertUser;
service.updateUser = updateUser;
service.getUserById = getUserById;
module.exports = service;

function getListUsers (callback){     
    User.find({}, function (err, users) {
        var newUsers = [];
        users.forEach(function(value){
            var newUser = {
                name: value.name,
                email: value.email,
                created_at: value.created_at,
                updated_at: value.updated_at,
                roles: value.roles,
                devices: value.devices,
                _id:value._id,
                imageAvatar: value.imageAvatar,
                regId: value.regId
            }
            newUsers.push(newUser);
        });
        
        if (err) {logger.error(err.message)}
        if(newUsers[0]==null){
            callback("Não há usuários na base de dados")
        }else {
        callback(newUsers);
        }
        });
  
}

function getUserById (id, callback){

    User.findById({ _id: id }, function(err, user) {
        if (err) {logger.error(err.message)};
        if (user == null){
            callback(false);
        }else {
            user.password;
            callback(user);
        }
    });
}

function getUser (username, callback){
    User.findOne({ username: username }, function(err, user) {
        if (err)  {logger.error(err.message)};
        if (user == null){
            callback("Usuário não encontrado");
        }else {
            callback(user);
        }
    });
}

function deleteUser (id, callback){
   User.findOne({_id: id}, function(err, user) {
  if (err)  {logger.error(err.message)}
  if (user == null ){ 
        callback("Usuario não encontrado")
  }else {
      User.remove(user,function(err) {
         if (err) {callback(err)
                     
                 }else{
                Device.update({users : [user._id]},{ $pull: {users:{$in:[user._id]} }},{multi:true}, function(err, device){
                    console.log("Erro"+err); 
                });
                     callback('Usuário deletado com sucesso!');
                 }
      });
  }
});
}

function insertUser (user, callback){
    user.save(function (err) {
		if (!err) {
			callback("User "+user.name +" created");
		} else {
			//TODO: return page with errors
			callback("Erro ao inserir usuário: \n"+err.errmsg);
		}
	});
}

function login(username, password, callback){
    User.findOne({username: username}, function(err, user){
        if (err){
            callback(err);
        }else if(user != null){
                user.comparePassword(password, function(isMatch) {
            
                if (isMatch == true){
                   callback(true);
                }else {
                    callback(false);
                }
            });
        }else {
            callback(false);
        }
    })
}

function updateUser (user, callback){
    if (user.username == null){
        callback("Username Inválido");
    }
    
    User.findOne({username: user.username}, function(err, userToUpdated) {
        if(err)
            callback( err);
       
        if (userToUpdated == null){
            callback("Usuário não existe");
        }
        
        if (user.password == null || user.name == 'undefined')
                    user.password = userToUpdated.password;
                 if (user.name == null || user.name == 'undefined')
                    user.name = userToUpdated.name;   
                    
        userToUpdated.comparePassword(user.password, function(isMatch) {
            if (user.name == userToUpdated.name && isMatch){
                callback("Nada a atualizar");
            }else {
                if(user.devices != null){
                    userToUpdated.devices = user.devices;
                }
              
                // change the users atributtes
                userToUpdated.name = user.name;
                userToUpdated.password = user.password;

                // save the user
                
                userToUpdated.save(function(err) {
                    if(err)
                    {
                        callback("Erro ao atualizar usuário: \n"+err.errmsg);
                    };
                
                    callback('User successfully updated!');
                });
            }
        });
    });
}

