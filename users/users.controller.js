/** users.controller.js
 * This file have all methods to handler external requests about users
 */
 
//Dependencies
var express = require('express');
var userService = require('./users.service');
var constants = require('./users.constants.json')
var User = require('./users.schema')
module.exports = {
    
    getListUsers:function (req, res, next) {
         userService.getListUsers(function (user) {
            if (user) {
                res.json(user);
            } else {
                
                return res.status(400);

            }
        })
        
    },
    getUser:function (req, res, next) {
        userService.getUser(req.params.username, function(result){
            if (result){
                res.send(result);
            }else {
                res.sendStatus(404);
            }
            
        });
        
    },
    deleteUser:function (req, res, next) {
     
         userService.deleteUser(req.params.username, function(result){
            
            if(result){
                res.send(result); 
            }else {
                res.status(400); 
            }
        })  
        
    },
    
    
    updateUser:function (req, res, next) {
     var name = req.body.name;
        var email = req.body.email;
        var password = req.body.password;
        var username = req.body.username;
        var devices = null;
        if (req.body.devices){
        var devices = req.body.devices;
        }
      
            var newUser = new User({
                name: name,
                email: email,
                password: password,
                username: username,
                devices: devices
            })
            userService.updateUser(newUser, function(result) {
                 
            if(result){
                res.send(result); 
            }else {
                res.status(400); 
            }
            });
    }
}
    
