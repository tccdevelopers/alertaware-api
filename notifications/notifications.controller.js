
var devicesService = require('../devices/devices.service');
var userService = require('../users/users.service');
var sendFunction = require ('./notifications.service');
var historicService = require('../sendHistoric/historic.service');
var Historic = require('../sendHistoric/historic.schema');
module.exports = {
    
        getSendPushNotification:function(req,res,next){
            var message = req.body.message;
            var title = req.body.title;
            var userId = req.body.user._id;
            var priority = Math.floor(Math.random() * 3) + 1 ;
            var send_way = "push";
            console.log(message);
            var newHistoric = new Historic({
                message:message,
                title:title,
                users:[userId],
                priority: priority,
                send_way:send_way
            })
            var credentials = {
                IonicApplicationID : "78379771",
                IonicApplicationAPItoken : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI2M2UxNzMzYy00MDBjLTRhNDItYWM1ZC1iNWEzNTAxZDcxYjcifQ.wE8GVoLaol0Ie_ai3S0AYLYtGxddiOCl5CkTZ2te0R4"
            };
            
            userService.getUserById(userId, function(user){
                var devices = user.devices;
                devices.forEach(function(doc){
                    devicesService.getDeviceById(doc, function(device){
                         var notification = {
                                "tokens": [device.registrationId],
                                "profile": "alertaware",
                                "notification": {

                                "android": {
                                "data":{
                                 "style": "inbox",
                                "summaryText": "Você tem %n% notificações",
                                "image": "https://d30y9cdsu7xlg0.cloudfront.net/png/194977-200.png",
                                "title": title,
                                "message": message
                                },
                               
                            },
                            "ios": {
                            "title": title,
                            "message": message
                            } 
                            }
                        };
                         sendFunction.sendMessage(credentials,notification, function(result){
                                console.log(result);
                                historicService.saveHistoric(newHistoric,function(response){
                                    console.log(response);
                                })
                                
                            
                        });
                        
                    })
                })
            })
            
            res.send("Mensagem enviada");
                
             },
             
        
        sendNotification:function(req,res, next){
        
        var credentials = {
            IonicApplicationID : "5660dc65",
            IonicApplicationAPItoken : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIwNmE0NjE2OS04OTRhLTQxNGUtODM1Ny1mMzZiNTg0MDA0MmMifQ.KmkjwtquS6DoJTrBayiJ7sGoNkRB3eFub5thk31_Wqw"
        };
        var notification = {
            "tokens": [req.body.registrationId],
            "profile": "test",
            "notification": {
                "title": req.body.title,
                "message": req.body.message,
                "android": {
                    "title": "Hey",
                    "message": "Hello Android!"
                },
            "ios": {
            "title": "Howdy",
            "message": "Hello iOS!"
            } 
            }
        };

        sendFunction.sendMessage(credentials,notification, function(result){
            console.log("result"+result);
            res.json(result);
        });
    
    }    
}