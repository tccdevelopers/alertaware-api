//users.service.js
/**
 * 
 *Here is the file with all methods to create, retrieve, modify and delete user(s) entities from database 
 *
 */

 var Device = require("./devices.schema");
 var User = require("../users/users.schema");
 //Service's array to export
var userService = require('../users/users.service')
var service = {};


service.getListUserDevice = getListUserDevice;
service.getListAllDevices = getListAllDevices;
service.getDevice = getDevice;
service.deleteDevice = deleteDevice;
service.getDeviceById = getDeviceById;
service.registerDevice = registerDevice;
module.exports = service;
//  if (user){
//         Device.find({_id: id}, function (err, devices) {
//         if (err) throw err;
//         if(devices[0]==null){
//             callback("Não há devices cadastrados para esse usuário")
        
//         callback(devices);
//          });
//         }else {
//             callback("Usuário não existe");
//         }
//     });


function getListUserDevice (id, callback){     
            console.log(id);
            userService.getUserById(id, function(user){
                Device.find( {users: {$in:[user._id]}}, function(err, device) {
                if (err) throw err;
                if (device == null){
                    callback("Dispositivo não encontrado");
                }else {
                    callback(device);
                }
        });
    })
        
}


function getDevice (registrationId, callback){
    console.log("DeviceId: "+registrationId);
    Device.findOne({ registrationId: registrationId }, function(err, device) {
        if (err) callback(err);
        if (device == null){
            callback(null);
        }else {
            callback(device);
        }
    });
}

function getDeviceById (_id, callback){
    console.log("DeviceId: "+_id);
    Device.findOne({ _id: _id }, function(err, device) {
        console.log(device);
        if (err) callback(err);
        if (device == null){
            callback(null);
        }else {
            callback(device);
        }
    });
}

function getListAllDevices (callback){
    Device.find({ }, function(err, device) {
        if (err) throw err;
        if (device == null){
            callback("Dispositivo não encontrado");
        }else {
            callback(device);
        }
    });
}

function deleteDevice (registrationId, callback){

    Device.findOne({ registrationId: registrationId }, function(err, device) {
        if (err) throw err;
        console.log(device);
        if (device == null ){ 
            callback("Dispositivo não encontrado")
        }else {
            var id = device._id;  
             Device.remove(device,function(err, deviceRemoved) {
                 if (err) {
                     callback(err)
                 }else{
                 User.find({}, function(err, users){
                    users.forEach(function(doc) {
                        // Find and remove item from an array
                        var i = doc.devices.indexOf(device._id);
                        if(i != -1) {
                        	doc.devices.splice(i, 1);
                        }
                      
                      var userSave = new User(doc);
                    
                     userSave.save(function(err){
                         console.log(err);
                     });
                    });
                 });
                callback('Dispositivo deletado com sucesso!');
                 }
            });
        }
    });
}

//Aqui nessa função deve ser assinado o dispositivo ao documents do usuário e por ultimo chama a função assignUserToDevice
/*
Celular pede para fazer registro, caso ele não tenha irá cadastrar
Caso tenha o deviceID deve verificar se o usuário é diferente, se sim chamar a assignUserToDevice e naquele usuário que mudou botar o dispositivo 
dele ou acrescentar mais um
Caso o deviceID e o usuário sejam o mesmo, atualizar o registrationID
Caso o device seja o mesmo mas o usuário nao e nem o registrationID criar um novo registro
Sempre criar um novo registro quando o usuário for não tiver aquele dispositivo!

*/

function registerDevice (userId, device, callback){
   var newDevice = new Device(device)
    //usar valueof pra transforma o array numa string
    Device.findOne({deviceId:device.deviceId}, function(err,deviceMongo){
        //
        userService.getUserById(userId, function(user){
            if(user == false){
            callback("Usuário não encontrado");
            }else {
                
                            
                if (deviceMongo != null && deviceMongo.registrationId == newDevice.registrationId && ((deviceMongo.users.toString()+"").indexOf(userId+"") > -1)){
                     callback('Device já existe');
                }else if(deviceMongo != null &&  deviceMongo.registrationId == newDevice.registrationId && (~(deviceMongo.users.toString()+"").indexOf(userId+"") > -1)){
                    deviceMongo.users.push(userId);
                    deviceMongo.save(function(err){
                        if (err) callback(err);
                        user.devices.push(deviceMongo._id);
                            userService.updateUser(user, function(response){
                                console.log(response);
                            })
                        callback("Atualizado device com novo usuário");
                    })
                }else if (deviceMongo != null){
                    if((deviceMongo.users.toString()+"").indexOf(userId+"") > -1){
                    deviceMongo.registrationId = newDevice.registrationId;
                    console.log("teste? "+deviceMongo.registrationId);
                    deviceMongo.save(function(err){
                        if (err) callback(err);
                        callback("Atualizado device com novo registrationId");
                    })
                    }else {
                        
                      deviceMongo.registrationId = newDevice.registrationId;
                      deviceMongo.users.push(userId);
                       user.devices.push(deviceMongo._id);
                            userService.updateUser(user, function(response){
                                console.log(response);
                            })
                            deviceMongo.save(function(err){
                        if (err) callback(err);
                        
                        callback("Atualizado device2 com novo registrationId");
                    })
                    }
                    
                    
                } else {
                    newDevice.users.push(userId);
                    newDevice.save(function(err, device){
                        console.log("Simplesmente salvou");
                        if (!err) {
                            console.log(device._id);
                            user.devices.push(device._id);
                            userService.updateUser(user, function(response){
                                console.log(response);
                            })
                            callback("Salvo com sucesso");
            
                        } else {
                            callback("Falha ao inserir");
            
                        }
                    });
                }
            }
        });
    });
}



