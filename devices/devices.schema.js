//device.js
/**
 *Device Schema 
 * 
 */
var User = require('../users/users.schema')
var mongoose = require('mongoose');
 
var deviceSchema = mongoose.Schema({ 
    deviceId        :{ type: String, index: { unique: false }},
    registrationId  :{ type: String, index: { unique: false }},
    users          :[{ type: mongoose.Schema.Types.ObjectId, ref: 'User', index: { unique: true}}],
    celNumber:String,
    updated_at: Date,
    created_at: Date
});
deviceSchema.index({registrationId:1, users:1}, { unique: true });

 
var Device = mongoose.model('device', deviceSchema);
      
deviceSchema.pre('remove', function (next) {
    var device = this;
    console.log("Entrou aqui tb");
    
});


deviceSchema.pre('save', function(next) {
  // get the current date
  var currentDate = new Date();
  
  // change the updated_at field to current date
  this.updated_at = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = currentDate;

  next();
});

deviceSchema.pre('update', function(next) {
  // get the current date
  var currentDate = new Date();
  
  // change the updated_at field to current date
  this.updated_at = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = currentDate;

  next();
});

module.exports= Device;


