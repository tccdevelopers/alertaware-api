/** devices.controller.js
 * This file have all methods to handler external requests about devices
 */
 
//Dependencies
var express = require('express');
var deviceService = require('./devices.service');
var constants = require('./devices.constants.json')
var Device = require('./devices.schema')


module.exports = {
    getListAllDevices:function (req, res, next) {
         deviceService.getListAllDevices(function (devices) {
            if (devices) {
                res.json(devices);
            } else {
                res.sendStatus(404);
            }
        })
        
    },
    
    getListUserDevice:function (req, res, next) {
        var id = req.params.userId;
        if ( typeof id  == 'undefined' || id == 'null' || id == null) {
            res.json("Parâmetros invalidos");
        } else if (!id.trim()) {
            res.json("Erro - Parâmetro vazios");
        } else {
        deviceService.getListUserDevice (id,function(devices){
            if (devices) {
                res.json(devices);
            } else {
                res.sendStatus(404);
            }
        })
        }
        
    },
    getDevice:function (req, res, next) {
        
        var id = req.params.registrationId;
        if ( typeof id  == 'undefined' || id == 'null' || id == null) {
            res.json("Parâmetros invalidos");
        } else if (!id.trim()) {
            res.json("Erro - Parâmetro vazios");
        } else {
        deviceService.getDevice (id,function(device){
            if (device) {
                res.json(device);
            } else {
                res.sendStatus(404);
            }
        })
        }
        
    },
    
    deleteDevice:function (req, res, next) {
        var registrationId = req.params.registrationId;
        if ( typeof registrationId  == 'undefined' || registrationId == 'null' || registrationId == null) {
            res.json("Parâmetros invalidos");
        } else if (!registrationId.trim()) {
            res.json("Erro - Parâmetro vazios");
        } else {
                deviceService.deleteDevice(registrationId, function(result){
                if(result){
                    res.send(result); 
                }else {
                    res.status(400); 
                }
            })  
        }
    },
    
    registerDevice:function (req, res, next) {
        
        var newDevice = new Device();
            console.log(req.body);
         newDevice.deviceId = req.body.deviceId;
         newDevice.registrationId = req.body.registrationId;
         var userId = req.body.userId;
         newDevice.celNumber = req.body.celNumber;
        if ( typeof newDevice.deviceId  == 'undefined' ||  typeof newDevice.registrationId  == 'undefined' || typeof userId == 'undefined'|| typeof newDevice.celNumber == "undefined") {
           res.json("Parâmetros invalidos");
        } else if (!newDevice.deviceId.trim() || !newDevice.registrationId.trim() || !userId.trim() || !newDevice.celNumber.trim() ) {
         
             res.json("Erro - Parâmetro vazios");
        } else {
            deviceService.registerDevice(userId, newDevice, function(result) {
                res.json(result);
            });
        }
    }
}
    
    