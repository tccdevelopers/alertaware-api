 var Device = require("../devices/devices.schema");
 var User = require("../users/users.schema");
 var Historic = require("./historic.schema");
 //Service's array to export
var userService = require('../users/users.service')
var service = {};


service.getListUserHistoric = getListUserHistoric;
service.getHistoric = getHistoric;
service.saveHistoric = saveHistoric;
module.exports = service;

function getListUserHistoric (id, callback){     
            userService.getUserById(id, function(user){
                Historic.find( {users: {$in:[user._id]}}, function(err, historic) {
                if (err) throw err;
                if (historic == null){
                    callback("Usuário sem notificações");
                }else {
                    callback(historic);
                }
        });
    })
}

function getHistoric (_id, callback){
    Historic.findOne({ _id: _id }, function(err, historic) {

        if (err) callback(err);
        if (historic == null){
            callback(null);
        }else {
            callback(historic);
        }
    });
}

function saveHistoric (historic, callback){
    historic.save(function (err) {
		if (!err) {
			callback("Notificação salva");
		} else {
			//TODO: return page with errors
			callback("Erro ao inserir Notificação: \n"+err.errmsg);
		}
	});
}