//device.js
/**
 *Device Schema 
 * 
 */
var User = require('../users/users.schema');
var Device = require('../devices/devices.schema');
var mongoose = require('mongoose');
 
var historicSchema = mongoose.Schema({
    devices : [{type: mongoose.Schema.Types.ObjectId, ref: 'Device', index: {unique: true}}],
    users   :[{ type: mongoose.Schema.Types.ObjectId, ref: 'User', index: { unique: true}}],
    message: String,
    title: String,
    send_way:String,
    priority:Number,
    image:String,
    updated_at: Date,
    created_at: Date
});

var Historic = mongoose.model('historicSend', historicSchema);

historicSchema.pre('save', function(next) {
  // get the current date
  var currentDate = new Date();
  
  // change the updated_at field to current date
  this.updated_at = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = currentDate;

  next();
});

historicSchema.pre('update', function(next) {
  // get the current date
  var currentDate = new Date();
  
  // change the updated_at field to current date
  this.updated_at = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = currentDate;

  next();
});

module.exports= Historic;


