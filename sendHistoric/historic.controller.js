/** historic.controller.js
 * This file have all methods to handler external requests about notifications historic
 */
 
//Dependencies
var express = require('express');
var historicService = require('./historic.service');
var Historic = require('./historic.schema')


module.exports = {
    getListUserHistoric:function (req, res, next) {
        var id = req.params.userId;
        console.log(id);
        if ( typeof id  == 'undefined' || id == 'null' || id == null) {
            res.json("Parâmetros invalidos");
        } else if (!id.trim()) {
            res.json("Erro - Parâmetro vazios");
        } else {
        historicService.getListUserHistoric (id,function(historic){
            if (historic == null || !historic.length){
                res.sendStatus(404).json("Histórico vazio para esse usuário")
            }else if (historic) {
                
                res.json(historic);
            } else {
                res.sendStatus(404);
            }
        })
        }
        
    },
    getHistoric:function(req, res, next){
         var id = req.params.id;
        if ( typeof id  == 'undefined' || id == 'null' || id == null) {
            res.json("Parâmetros invalidos");
        } else if (!id.trim()) {
            res.json("Erro - Parâmetro vazios");
        } else {
        historicService.getHistoric(id,function(historic){
            if (historic == null){
                 res.sendStatus(404).json("Notificação não encontrada no histórico desse usuário");
            
            }else if (historic) {
                res.json(historic);
            } else {
                res.sendStatus(404);
            }
        })
        }
    },
    saveHistoric:function(req,res,next){
        var newHistoric = new Historic();
         newHistoric.title = req.body.title;
         newHistoric.message = req.body.message;
         newHistoric.device = req.body.device;
         newHistoric.send_way = req.body.send_way;
         newHistoric.priority = req.body.priority;
         newHistoric.users = req.body.users;
         newHistoric.image = req.body.image;
        if ( typeof newHistoric.title  == 'undefined' ||  typeof newHistoric.message  == 'undefined' || typeof newHistoric.priority == 'undefined') {
           res.json("Parâmetros invalidos");
        } else if (!newHistoric.title.trim() || !newHistoric.message.trim() ) {
             res.json("Erro - Parâmetro vazios");
        } else {
            historicService.saveHistoric(newHistoric, function(result) {
                res.json(result);
            });
        }
    }
}