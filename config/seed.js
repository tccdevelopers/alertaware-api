'use strict';

var logger   = require('mm-node-logger')(module);
var mongoose = require('mongoose');
var User     = require('../users/users.schema');
var userService = require('../users/users.service');
var Device = require('../devices/devices.schema');

var newUser1 = new User({
    username: "brunobrixius",
    password: "123456",
    name: "Bruno Brixius",
    email: "brunobrixius@gmail.com",
    regId: "14028",
    provider: 'local',
    imageAvatar: "https://scontent-mia1-1.xx.fbcdn.net/v/t1.0-9/1240134_1244648568895627_614511628756310536_n.jpg?oh=c2a2359ca91a97bb48728e7f3bd57385&oe=57CD3152",
    roles: ["aluno"]
})

var newUser2 = new User({
    username: "adrianlemess",
    password: "123456",
    name: "Adrian Lemes",
    email: "adrianlemess@gmail.com",
    regId: "15722",
    provider: 'local',
    imageAvatar: "https://scontent-mia1-1.xx.fbcdn.net/v/t1.0-9/11248065_895824427150620_8114904638596723718_n.jpg?oh=def7686afa3abe2c28dfe2611909bae8&oe=57D682A2",
    roles: ["aluno"]
})


var newUser3 = new User({
    username: "dionataferraz",
    password: "123456",
    name: "Dionata Ferraz",
    email: "dionataferraz@gmail.com",
    regId: "15726",
    provider: 'local',
    imageAvatar: "https://scontent-mia1-1.xx.fbcdn.net/v/t1.0-9/10376298_541197462669629_7207742724890548888_n.jpg?oh=35888c57efeee22528858722b3b24870&oe=57C6D555",
    roles: ["aluno"]
})
User.find({}).remove(function() {
    newUser1.save(function(err) {
        if (err) console.log("Erro para popular usuário 1: "+err);
            newUser2.save(function(err2){
                 if (err) console.log("Erro para popular usuário 2: "+err2);
                 newUser3.save(function(err3){
                      if (err) console.log("Erro para popular usuário 3: "+err3);
                      console.log("Finalizado seed de usuários")
                 });
            });
        });
});

User.find({}).remove(function(err){
    if(err) logger.log(err);
})

Device.find({}).remove(function(err){
    if (err) logger.log(err);
})