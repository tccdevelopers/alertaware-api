//controllers/routes.js
/**
 *Mapping routes to send a request to controllers 
 * 
 */
var express = require('express');
var userController = require('../users/users.controller');
var notificationController = require('../notifications/notifications.controller');
var deviceController = require('../devices/devices.controller');
var router = express.Router();
var authenticationController = require('../authentication/authentication.controller');
var historicController = require("../sendHistoric/historic.controller");

router.get('/', function(req, res){
    res.send("API da aplicação Alert Aware")
})

//authentication
router.post('/auth/signin', authenticationController.signin);    
router.post('/auth/signup', authenticationController.signup);
router.get('/auth/logout', authenticationController.signout);

//Users
router.get('/users', userController.getListUsers);
router.get('/user/:username', userController.getUser);
router.delete('/user/:username', userController.deleteUser);
router.put('/user', userController.updateUser);

//Notification Historic
router.get('/listHistoric/:userId',historicController.getListUserHistoric);
router.get('/historic/:id',historicController.getHistoric);
router.post('/historic',historicController.saveHistoric);

//Devices
router.get('/devices', deviceController.getListAllDevices);
router.get('/devices/:userId', deviceController.getListUserDevice);
router.get('/device/:registrationId', deviceController.getDevice);
router.delete('/device/:registrationId', deviceController.deleteDevice);
router.post('/device', deviceController.registerDevice);

//Notification
router.post('/notification', notificationController.getSendPushNotification);

module.exports = router;  